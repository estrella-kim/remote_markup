lazyside Front-end 개발자를 위한 MarkUp 규칙입니다. 

### 1. 문서의 구조와 표현을 분리합니다
> 1. 문서의 마크업 언어(HTML)와 화면 표시(CSS)언어를 본래의 목적에 맞게 `최대한 분리`합니다.
> 2. 의미 구조를 마크업 하는 경우 HTML 요소와 속성을 이용 합니다. 표현을 위한 HTML 요소와 속성의 사용은 지양합니다. 단, 테이블 셀의 경우 다양한 너비의 고정폭을 지닐 수 있는데 이것을 모두 CSS로 지정하는 것은 기대했던 효과에 대한 이점이 없으므로 예외의 경우를 허용합니다.
> 3. 문서의 표현에 관한 속성은 외부(External) CSS문서에서 정의 합니다. HTML문서 Head에 CSS를 삽입하는 Internal 방식과 HTML요소에 직접 기술하는 Inline 방식의 사용은 되도록 지양합니다.

### 2. 의미에 맞는 시멘틱한 태그 사용을 지향합니다.
>  각 엘리먼트의 역할과 의미에 맞는 태그를 사용합니다. 예) 순서가 있는 목록의 경우, `<ol>`, `<li>`태그를 사용합니다. 

### 2. 다양한 브라우저의 호환을 지원합니다
제작된 모든 문서는 서비스의 종류에 관계없이 모든 브라우저에서의 호환을 지원하여야 합니다. 한편 프로젝트별 스펙 명세서에 의하여 정해진 기준 브라우저가 있는 경우 완성된 문서는 가능한 범위 내에서 시안과 정확하게 그리드를 일치시켜야 합니다. 그 밖의 브라우저에서는 주요한 배치등이 깨지지 않고 시각적으로 다르다는 것을 인지할 수 없는 정도의 수준까지 일치시킵니다. 기준 브라우저가 정해지지 않은 경우 해당 프로젝트 개발시점 기준으로 가장 접속률이 높은 브라우저가 기준 브라우저가 됩니다. 비공개 베타버전으로 릴리즈된 브라우저는 호환 대상에 포함시키지 않습니다.

### 3. main(index.html) 파일은 가장 최상위 디렉토리에 두고 서브html파일은 view폴더 안에서 관리합니다. 


```
[ 예시 ]



[festival]
  │
  ├[view]
  │  ├ login.html
  │  ├ notice.html
  │  └ introduction.html
  ├[css]
  ├[js]
  ├[static]
  └ index.html
  
      
```

### 4. 네이밍(naming)
> - 이름은 영문 소문자, 숫자, 언더스코어(_)로 작성합니다.
> - 시작이름은 영문 소문자로만 작성합니다.
> - 파일이름

> - id는 사용하지 않고 오직 class만을 사용합니다.  
    
```html
<html>
    <body>
        <header class="header">
            
        </header>
        <section class="section">
            
        </section>    
    </body>
</html>
``` 

> - 상위 태그 내부에 들어갈 영역의 class의 이름은 각 기능으로 서술합니다.  
    상위 태그와 자식 태그(기능)의 연결은 `__`(더블언더바)를 사용합니다.    
    
```html
<html>
    <body>
        <header class="header">
            <div class="header__gnb">
                <!--특정 기능을 수행하는 컴포넌트를 만들 경우 __ 사용합니다-->
            </div>         
        </header>
        <section class="content">            
            <div class="content__menu">
                <div class="content__menu__login">
                    <!--특정 기능을 수행하는 컴포넌트를 만들 경우 __ 사용합니다-->
                </div>
            </div>
        </section>
    </body>
</html>
``` 

> - `_`는 순서를 가지거나 다중의 엘리먼트에 네이밍을 부여하는 경우에 사용합니다.(즉 `_`은 정렬을 위해서 사용)  

```
	<html>
		<body>
			<div class="header">
			</div>
			<div class="content">
				<div class="content__wrap">
					<ul class="content__wrap__articles-box">
						<li class="content__wrap__articles-box_1"></li>
						<li class="content__wrap__articles-box_2"></li>
						<li class="content__wrap__articles-box_3"></li>
						<li class="content__wrap__articles-box_4"></li>
					</ul>
				</div>
			</div>
			<div class="footer">
			</div>
		</ody>
	</html>
	
```
> - `-`은 자식의 이름이 너무 길거나 읽기가 어려울 경우 사용합니다. (즉 `-`은 네이밍을 위해서 사용)
```html
    <li class="content__wrap__articlesbox_1"></li> // articlesbox가 읽기 어렵다(X)
    <li class="content__wrap__articles-box_1"></li> // articles-box를 읽기 쉽게 만들었다.(O)
```

> - `--`은 수정하거나 같은 컴포넌트로 다른 버전이나 수정(변형판)을 할 경우 사용합니다. (즉 `--`은 수정을 위해서 사용)
```html
    <li class="content__wrap__articles-box_1"></li> // 기본 articles-box
    <li class="content__wrap__articles-box--festival"></li>// festival버전의 articles-box
```
        
### 5. 주석
> Doxygen을 이용한 주석 규칙을 사용합니다.
## 주석 작성은 명령어 리스트에 명시하여 작성합니다.  
```html
/**
* @명령어
*/
 

/**
* @file require.config.js
* @brief 간략한 설명
* @details 자세한 설명
*/
```

>1.  `@file` - 파일표시
>1.  `@author` - 작성자
>1.  `@brief` - 간략한 설명
>1.  `@details` - 자세한 설명
>1.  `@param` - 변수 설명
>1.  `@return` - 리턴 설명
>1.  `@see` - 참고사항, 주석을 볼 때 추가적으로 확인해야 할 사항들 작성
>1.  `@todo` - 추가적으로 처리해야할 사항들 작성
>1.  `@deprecated` - 삭제 예정 표시
>1.  작성순서는 `@file`부터 `@deprecated`까지 순서대로 작성하며 @file부터 @brief까지는 필수로 작성하고 나머지는 옵션으로 작성합니다.


